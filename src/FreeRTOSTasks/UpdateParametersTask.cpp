#include "UpdateParametersTask.hpp"

void UpdateParametersTask::execute() {
    TaskHandle_t updateParametersHandle = xTaskGetHandle(TaskName);

    while (true) {
        PlatformParameters::reportParametersUnusedStack.setValue(
                uxTaskGetStackHighWaterMark(updateParametersHandle));
        PlatformParameters::availableHeap.setValue(
                static_cast<uint16_t>(xPortGetFreeHeapSize()));
        PlatformParameters::mcuBootCounter.setValue(
                static_cast<uint16_t>(BootCounter::GPBRRead(BootCounter::BootCounterRegister)));
        PlatformParameters::mcuSysTick.setValue(static_cast<uint64_t>(xTaskGetTickCount()));
        vTaskDelay(pdMS_TO_TICKS(delayMs));
    }
}
